# hapi-postgresql-prod-identity-resolver
# db.t3.medium
# postgresql
# Engine version 9.6.22
# License = Postgresql
# vcpu 2
# RAM 4GB
# Encryption = not enabled
# Storage = General Purpose SSD (gp2)
# Storage Autoscaling = enabled
# Max Storage threshold = 200 GiB
# Performance insights enabled = NO
# CloudWatc Logs = PostgreSQL
#
# Parameter Group = hapipsql96
# Option Group = default:posggresql-9-6
# Auto minor version upgrade = enabled
# Maint Window = mon:05:11-mon:05:41 UTC (GMT)

# tags
Name HAPI_PROD PostreSQL 
workload-type other
