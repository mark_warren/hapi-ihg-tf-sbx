variable "region" {
  description = "AWS Region; us-east-1, us-east-2"
  default     = "us-east-1"
}

variable "short_region" {
  description = "AWS Short Region; use1, use2"
  default     = "use1"
}

variable "environment" {
  description = "environment; dev, demo, prod."
  default     = "dev"
}

variable "overwrite_parameters" {
  description = "Overwrite an existing parameter."
  default     = true
}
