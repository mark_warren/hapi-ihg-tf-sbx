terraform {
  # backend "s3" {}
  backend "local" {}

  required_version = ">= 0.12"
  required_providers {
    aws = "~> 3.61"
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "hapi"
}

# https://github.com/mozilla/sops
# provider "sops" {}
#
# data "sops_file" "secrets_file" {
#   source_file = "${path.module}/secrets/${var.environment}-${var.short_region}-xxxxx.json"
# }

data "aws_kms_key" "default_kms_key" {
  key_id = "alias/aws/ssm"
}

resource "aws_ssm_parameter" "hms_gateway_endpoint_secret" {
  name      = "/${var.environment}/hms-gateway/endpoint/secret"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["hms_gateway_endpoint_secret"] # KPcRxxxxxx
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_buffer_writer" {
  name      = "/${var.environment}/kafka/access-key/buffer-writer"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_buffer_writer"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_command_api" {
  name      = "/${var.environment}/kafka/access-key/command-api"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_command_api"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_flow_management" {
  name      = "/${var.environment}/kafka/access-key/flow-management"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_flow_management"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_generic_router" {
  name      = "/${var.environment}/kafka/access-key/generic-router"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_generic_router"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_hapi_producer" {
  name      = "/${var.environment}/kafka/access-key/hapi-producer"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_hapi_producer"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_ksql_server" {
  name      = "/${var.environment}/kafka/access-key/ksql-server"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_ksql_server"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_mongo_consumer" {
  name      = "/${var.environment}/kafka/access-key/mongo-consumer"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_mongo_consumer"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_oxi_producer" {
  name      = "/${var.environment}/kafka/access-key/oxi-producer"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_oxi_producer"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_producers_converter" {
  name      = "/${var.environment}/kafka/access-key/producers-converter"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_producers_converter"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_rlh_il_consumer" {
  name      = "/${var.environment}/kafka/access-key/rlh-il-consumer"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_rlh_il_consumer"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}

resource "aws_ssm_parameter" "kafka_access_key_sender" {
  name      = "/${var.environment}/kafka/access-key/sender"
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["kafka_access_key_sender"] # RSA private key
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}
