# OVERVIEW
+ This repo contains the terraform module used to deploy AWS Parameter Store entries.
+ It can leverage the Mozilla/SOPS plugin to encrypt sensitive values stored in the repo.

# SOPS
##### https://github.com/mozilla/sops

+ Install sops locally on your machine.
+ Install sops terraform plugin
+ Use the secrets.sh script to update encrypted values.
+ All TF secrets values should be stored in their corresponding env-short_region.json file. Ex; dev-use1.json
+ All other non-sensitive values AWS Param Store values can be defined directly in the main.tf or in the environment tfvars. Ex; environments/dev.tfvars

##### SOPS install on Mac OS with brew - Used to locally decrypt/encrypt the secrets file.

```sh
brew install sops
```
##### SOPS Terraform Provider Plugin - Used to decrypt secrets file and inject values into TF.
##### Required for running terraform locally
#### https://github.com/carlpett/terraform-provider-sops

```sh
mkdir -p ~/.terraform.d/plugins

curl -L -o ~/.terraform.d/plugins/terraform-provider-sops_v0.5.0_darwin_amd64.zip \
"https://github.com/carlpett/terraform-provider-sops/releases/download/v0.5.0/terraform-provider-sops_v0.5.0_darwin_amd64.zip"

unzip ~/.terraform.d/plugins/terraform-provider-sops_v0.5.0_darwin_amd64.zip \
-d ~/.terraform.d/plugins
```
For terraform 13.x copy the plugin to this directory on your mac.
```
~/.terraform.d/plugins/registry.terraform.io/hashicorp/sops/0.5.0/darwin_amd64
```

If you directory is not already created you will need to first create it as shown below:
```sh
mkdir -p ~/.terraform.d/plugins/registry.terraform.io/hashicorp/sops/0.5.0/darwin_amd64
```

Then copy the directory as follows:

```sh
cp -rf ~/.terraform.d/plugins/terraform-provider-sops_v0.5.0  ~/.terraform.d/plugins/registry.terraform.io/hashicorp/sops/0.5.0/darwin_amd64
```

#### See /scripts/secrets.sh to encrypt and decrypt
Usage of script: `source secrets.sh $env $region`
Where:
* `$env` - abbreviated name for the environment where the changes should be applied.
* `$region` - is the AWS Region where the changes should be applied into

Ex.
```sh
source secrets.sh dev us-east-1
```

The getparams.sh script is a tool that will allow you to quickly query existing Param Store
entries to verify their values. See script for usage.

#### Adding a new Parameter to parameter store
To create or modify the name of an existing parameter in parameter store. Find the `main.tf` file under the `terraform/modules/params` directory under the project root directory and add a new parameter store param similiar to the following.

```

resource "aws_ssm_parameter" "new_param_store_entry" {
  name      = "/${var.environment}/kafka/access-key/new-param-store-entry" # Set correct Path.
  type      = "SecureString"
  value     = data.sops_file.secrets_file.data["new_param_store_entry"]
  overwrite = var.overwrite_parameters
  key_id    = data.aws_kms_key.default_kms_key.arn
  # tags      = var.tags
}
```

The terraform resource name `new_param_store_entry` should be replaced with the actual name of the parameter that you wish to create and should also match the same name in the sops secrets file.
