#!/usr/bin/env sh
#
# This script uses SOPS to encrypt and decrypt a local file. This file is then used in
# terraform with the SOPS plugin to decrypt values and inject them in the different
# Parameter Store entries during terraform apply.
#
#
# Usage of script: source secrets.sh $env $region
# Ex.
# source secrets.sh dev us-east-1
# source secrets.sh dev us-east-2
#
# This is the KMS Key Name we use to encrypt the local SOPS file.
# This key should already exist in AWS
app_key=ihg # alias/dev/us-east-1/ihg

#
env=$1
region=$2
profile=hapi

export AWS_PROFILE=$profile

case ${region} in
    (us-east-1|us-east-2|us-west-2) ;;
    (*) printf >&2 '*** Error: Valid inputs are us-east-1|us-east-2|us-west-2 ...\n'; exit 1;;
esac

short_region=$(echo "$region" | sed -E 's/-(.)[^-]*/\1/g')

# SOPS only works with the KMS ARN, so we extract it from the KMS Alias and add it to our local env vars:
unset SOPS_KMS_ARN
# export SOPS_KMS_ARN=$(aws kms describe-key --key-id alias/$env/$region\/$app_key --region $region --profile $profile | jq -r '.KeyMetadata.Arn')
export SOPS_KMS_ARN=$(aws kms describe-key --key-id alias/dev/us-east-1/ihg --region us-east-1 --profile hapi | jq -r '.KeyMetadata.Arn')

echo "KMS ARN: $SOPS_KMS_ARN"
#
# Decrypt and edit json in default editor ($EDITOR), close and re-encrypt.
sops ../secrets/$env-$short_region.json

# Diplay unencrypted File
echo "#####################################################################################"
echo "            CONFIRM ALL ENTRIES BEFORE COMMITTING TO REPO!!"
echo "#####################################################################################"
echo "APP KEY: $app_key"
echo "AWS ENV: $env"
echo "AWS Region: $region"

sops -d ../secrets/$env-$short_region.json | jq
