#!/usr/bin/env sh
#
# Helper script to pull down current Param Store Entries
# Usage ex.
# sh getparams.sh prod

awscli=$(which aws)

# AWS credentials profile
profile=hapi-us

# prod, dev, demo
env=$1

declare -a ParamPaths=("kafka/access-key" "kafka/certificate" "logs/visualizer" "mongo" "postgresql" "redshift")
# AWS SSM Paths:
# kafka/access-key
# kafka/certificate
# logs/visualizer
# mongo
# postgresql
# redshift

for path in ${ParamPaths[@]}; do
  echo "Path: "$path
  $awscli ssm get-parameters-by-path \
  --path "/$env/$path/" \
  --profile $profile \
  --region us-east-1 \
  | jq -r '.Parameters[] | {Name, Value}'
done
