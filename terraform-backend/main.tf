# terraform state backup setup.
# used only once.

provider "aws" {
  region = "us-east-1"
  profile = "hapi"
}

data "aws_caller_identity" "current" {}

output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "caller_arn" {
  value = data.aws_caller_identity.current.arn
}

output "caller_user" {
  value = data.aws_caller_identity.current.user_id
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-state-ihg-use1"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = false
  }

  policy = file("s3-policy.json")


  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "block_public" {
 bucket = aws_s3_bucket.terraform_state.id

 block_public_acls       = false
 block_public_policy     = false
 ignore_public_acls      = false
 restrict_public_buckets = false
}


resource "aws_dynamodb_table" "terraform_state_locks" {
  name         = "terraform-state-locks"
  hash_key     = "LockID"
  read_capacity = 5
  write_capacity = 5

  attribute {
    name = "LockID"
    type = "S"
  }
}
