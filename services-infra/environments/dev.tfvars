environment = "dev"
region      = "us-east-1"
vpc_id      = "vpc-028524efeb0f1f68f"
account_id  = "016522611453"

# nat_gw = "nat-0710b2a1353d716bf"

# NGINX ASG AZs. One subnet per AZ.
private_subnets = ["subnet-0b07a89eb43803d81", "subnet-086fc52d4843245a4", "subnet-0e5a915a1b85e6870"]

# Bastion
public_subnets     = ["subnet-09b385a83de607c5e"]
allowed_public_ips = ["128.136.192.0/24"]

# Test app name
enable_app_1     = true
service_app_name = "alpine"

service_task_cpu    = 4096
service_task_memory = 16384

service_desired_task_count = 2
service_deploy_min_percent = 100
service_deploy_max_percent = 200

# Container specific parameters
sizing_options = {
  app_cpu          = 3584
  app_memory_init  = 512
  app_memory_limit = 16384
}


#AutoScalingGroup Attributes
asg_attributes = {
  min_capacity = 2
  max_capacity = 10
  policy = [{
    predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    target_value           = 60
    scale_in_cooldown      = 60
    scale_out_cooldown     = 60
    },
    {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
      target_value           = 60
      scale_in_cooldown      = 60
      scale_out_cooldown     = 60
    }
  ]
}
