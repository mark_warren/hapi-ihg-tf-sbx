## Service Infrastructure
- Terraform version - 0.13.5
- Terraform Backend - S3, DynamoDB
- ECS/Fargate - Services and Resources
- NGINX EC2 Cluster
- Other EC2 Servers/Clusters


## Terraform state Backend
1.) S3 Bucket
2.) DynamoDB for state lock.

## ECS Resources
1.) ECS Clusters: one per environment (root module)
hapi-prod, ec2-hapi-dev, hapi-demo, hapi-dev

2.) ECS Services: (hapi-prod) one per app. (fargate module)
producers-converter, flow-management, hapi-producer, command-property-dispatcher,
identity-resolver, mongo-consumer, hms-gateway, command-api, ows-adapter, key-keeper,
generic-router, hapi-admin-console, generic-transformer, ksql-server, rlh-il-consumer,
sender, logs-visualizer, repository-api

3.) Cloud Map - Namespace (per environment; dev, demo, prod). Services - one per app. (fargate module)
4.) Security-Groups - single SG for all hapi-${environment} ECS Services. "Prod-US_Private_SG" (root module)
5.) Task Definitions - (fargate module)
6.) Task Execution Role - arn:aws:iam::527421504631:role/ecsTaskExecutionRole - iam.tf
7.) ECS AutoScalingGroup Required - NO.
8.) VPC, SUBNET, CIDR Management (locals, env.tfvars, data source, etc.)
9.) Individual App variables - cpu, mem, hard/soft mem, desired count
10.) Individual App container definitions - environment/secrets. - (fargate module)
11.) CloudWatch log groups per app. - (fargate module)
12.)

## NGINX EC2 Cluster (module/ec2)
1.) Launch configuration (PROD-US NGINX NLB). t2.micro, AMI = ami-08bb639d05b9c4f3c
2.) SSH key
3.) ASG ?
4.) SGs - prometheus-exporter, HapIProdPublic_SG
5.) IAMRole - SSMRole
6.) VPC - hapiProdVPC
7.) Public IP Interface. ENIs.
8.) NLB - LB-NGINX-HTTPS-US (nginx public facing NLB)
9.) NLB Listener - TLS:443. SSL from ACM *.hapicloud.io
10.) NLB Target Group - TG-NGINX-NLB-HTTPS-EIP-US 443, PROD-US ASG NGINX 1.20.1 NLB HTTPS A1-3
11.) DNS/R53 - A Record, Internet Facing (ex. LB-NGINX-HTTPS-US-5695cde176a08839.elb.us-east-1.amazonaws.com)
