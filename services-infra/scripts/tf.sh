#!/usr/bin/env bash

if [ "$#" -lt 1 ]; then
    echo "*** Error: you must supply action (init|plan|apply|destroy|refresh|state)"
    exit 1
fi


action=$1
environment=dev
bucket=terraform-state-ihg-use1
#table=terraform-state-locks
shift

case ${action} in
    init)
    terraform init \
               -backend-config="bucket=terraform-state-ihg-use1" \
               -backend-config="dynamodb_table=terraform-state-locks" \
               -backend-config="key=dev.tfstate" \
               -backend-config="region=us-east-1" --upgrade .
    ;;
    plan|apply|destroy|refresh)
    terraform $action -var-file=../environments/global.tfvars -var-file=../environments\/$environment.tfvars
    ;;
    state)
    terraform $action "$@"
    ;;
    *)
    printf >&2 '*** Error: second arg must be one of (state|init|plan|apply|destroy|refresh) ...\n'
    exit 1
    ;;
esac
