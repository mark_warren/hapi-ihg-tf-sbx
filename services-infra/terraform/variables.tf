variable "account_id" {
  description = "AWS account ID"
  type        = any
}

variable "environment" {
  description = "The environment this resource will be deployed in, e.g. dev, demo, prod."
}

variable "region" {
  description = "AWS Region; us-east-1, us-east-2"
  default     = "us-east-1"
}

variable "short_region" {
  description = "AWS Short Region; use1, use2"
  default     = "use1"
}

variable "vpc_id" {
  description = "AWS VPC"
}

variable "private_subnets" {
  description = "Private subnets"
  type        = list
}

variable "public_subnets" {
  description = "Public subnets"
  type        = list
}

variable "allowed_public_ips" {
  description = "Public IPs allowed to access Bastion"
  type        = list
}



variable "service_cluster" {
  description = "service cluster"
  type        = any
  default     = "test"
}

variable "service_name" {
  description = "The name of the service_name"
  type        = string
  default     = "test"
}

variable "service_app_name" {
  description = "The name of the service_app_name"
  type        = string
  default     = "test"
}

variable "enable_app_1" {
  description = "Enable/disable the deployment of this app"
  type        = bool
  default     = false
}

# variable "app_key" {
#   description = "Main KMS Key for the project"
# }

# variable "namespace_id" {
#   description = "The name of the namespace"
#   type        = string
# }

variable "service_desired_task_count" {
  description = "service desired number of container tasks to deploy"
  type        = number
  # default     = 1
}

variable "service_deploy_min_percent" { #
  description = "The lower limit as a % of the service's desiredCount of the no. of running tasks that must remain running and healthy in a service during a deployment."
  type        = number
  # default     = 10
}

variable "service_deploy_max_percent" { #
  description = "The upper limit as a % of the service's desiredCount of the no. of running tasks that can be running in a service during a deployment."
  type        = string
  # default     = 100
}

variable "service_task_cpu" { #
  description = "The number of cpu units used by the task. If the requires_compatibilities is FARGATE this field is required."
  type        = number
  default     = 4096
}

variable "service_task_memory" { #
  description = "The amount (in MiB) of memory used by the task. If the requires_compatibilities is FARGATE this field is required."
  type        = number
  default     = 16384
}

variable "container_name" {
  description = "The name of the container"
  type        = string
  default     = "hapi"
}

variable "platform_version" { #
  description = "The Fargate platform version on which to run service"
  default     = "1.4.0"
}

variable "sizing_options" {
  description = "Fargate Container Parameters."
  type = object({
    app_cpu          = number // Container level CPU
    app_memory_init  = number // The amount (in MiB) of memory to present to the container
    app_memory_limit = number // The soft limit (in MiB) of memory to reserve for the container.
  })
}

# variable "service_version" { #
#   description = "service version to be deployed."
# }


variable "asg_attributes" {
  description = "ASG attributes for ECS Target Tracking scaling."
  type = object({
    min_capacity = number //The minimum autoscaling group value
    max_capacity = number //The maximum autoscaling group value
    policy = list(object({
      predefined_metric_type = string //An AWS Predefined metric type: ECSServiceAverageCPUUtilization or ECSServiceAverageMemoryUtilization
      target_value           = number //The target value for the metric.
      scale_in_cooldown      = number //The amount of time, in seconds, after a IN scaling activity completes and before the next IN scaling activity can start.
      scale_out_cooldown     = number //The amount of time, in seconds, after a OUT scaling activity completes and before the next OUT scaling activity can start.
    }))
  })
  default = {
    min_capacity = 1
    max_capacity = 5
    policy = [{
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
      target_value           = 85
      scale_in_cooldown      = 60
      scale_out_cooldown     = 60
      },
      {
        predefined_metric_type = "ECSServiceAverageCPUUtilization"
        target_value           = 85
        scale_in_cooldown      = 60
        scale_out_cooldown     = 60
      }
    ]
  }
}

variable "slow_start" { #
  description = "amount time for targets to warm up before the load balancer sends them a full share of requests. Default 0 (disabled)"
  type        = number
  default     = 0
}


variable "enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service."
  type        = bool
  default     = false
}
