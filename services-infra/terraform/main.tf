# tfenv 0.13.5
#
# ** ECS Clusters: one per environment
# hapi-prod, ec2-hapi-dev, hapi-demo, hapi-dev
#
# ** ECS Services: (hapi-prod) one per app. (fargate module)
# producers-converter, flow-management, hapi-producer, command-property-dispatcher,
# identity-resolver, mongo-consumer, hms-gateway, command-api, ows-adapter, key-keeper,
# generic-router, hapi-admin-console, generic-transformer, ksql-server, rlh-il-consumer,
# sender, logs-visualizer, repository-api
#
# ** Cloud Map - Namespace (per environment; dev, demo, prod). Services - one per app. (fargate module)
#
# ** Security-Group - single SG for all hapi-prod ECS Services. "Prod-US_Private_SG"
# ** Task Definitions - (fargate module?)
# ** Task Execution Role.

terraform {
  # backend "s3" {}
  backend "local" {}

  required_version = ">= 0.12"
  required_providers {
    aws = "~> 3.61"
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "hapi"
}

#############################################################################
# Security Groups
#############################################################################
// Test SG w/ port 8080 open
resource "aws_security_group" "cluster_sg_8080" {
  name        = "${var.environment}-sg"
  description = "Allow access to 8080"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"] # data.aws_subnet.private_subnets[each.key.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


############################################################################
# Cloud Map Namespace (dev, demo, prod)
############################################################################
resource "aws_service_discovery_private_dns_namespace" "cloudmap_namespace" {
  name        = var.environment
  description = "HAPI Namespace"
  vpc         = var.vpc_id
}


############################################################################
# Create ECS Cluster Name
############################################################################
resource "aws_ecs_cluster" "ecs_cluster" {
  name = "hapi-${var.environment}" # hapi-prod, ec2-hapi-dev, hapi-demo, hapi-dev

  setting {
    name  = "containerInsights"
    value = var.environment == "prod" ? "enabled" : "disabled"
  }

  tags = {
    Name = "hapi-${var.environment}"
    Env  = var.environment
  }
}

############################################################################
# Deploy ECS Fargate Applications
############################################################################
// Fargate App #1 - Alpine test
module "ecs_fargate_app_1" {
  source                     = "./modules/fargate/"
  count                      = 0 #var.enable_app_1 ? 1 : 0
  environment                = var.environment
  task_name                  = var.service_app_name
  namespace_id               = aws_service_discovery_private_dns_namespace.cloudmap_namespace.id
  account_id                 = var.account_id
  sizing_options             = var.sizing_options
  service_name               = var.service_app_name
  service_deploy_min_percent = var.service_deploy_min_percent
  service_deploy_max_percent = var.service_deploy_max_percent
  service_desired_task_count = var.service_desired_task_count
  security_groups            = [aws_security_group.cluster_sg_8080.id]

  container_definitions = templatefile("${path.module}/modules/fargate/templates/container-definition.json",
    {
      sizing_options = var.sizing_options
      environment    = var.environment
    }
  )
}

// Fargate App #2 - NGINX test
// https://gallery.ecr.aws/ubuntu/nginx
// public.ecr.aws/ubuntu/nginx:1.18-20.04_beta
module "ecs_fargate_app_2" {
  source                     = "./modules/fargate/"
  count                      = var.enable_app_1 ? 1 : 0
  environment                = var.environment
  task_name                  = "nginx-test" # var.service_app_name
  namespace_id               = aws_service_discovery_private_dns_namespace.cloudmap_namespace.id
  account_id                 = var.account_id
  sizing_options             = var.sizing_options
  service_name               = "nginx-test" # var.service_app_name
  service_deploy_min_percent = var.service_deploy_min_percent
  service_deploy_max_percent = var.service_deploy_max_percent
  service_desired_task_count = var.service_desired_task_count
  #security_groups            = aws_security_group.cluster_sg_8080.id
  security_groups = [aws_security_group.cluster_sg_8080.id]

  container_definitions = templatefile("${path.module}/modules/fargate/templates/nginx-test-definition.json",
    {
      sizing_options = var.sizing_options
      environment    = var.environment
      region         = var.region
      service_name   = "nginx-test"
    }
  )
}


#############################################################################
# BASTION
#############################################################################
module "bastion" {
  source                      = "./modules/bastion/"
  count                       = 1 # var.enable_app_1 ? 1 : 0
  application                 = "${var.environment}-bastion"
  environment                 = var.environment
  bastion_asg_desired_size    = 1
  bastion_asg_max_size        = 1
  bastion_asg_min_size        = 1
  bastion_image_id            = "ami-01cc34ab2709337aa" # amzn2-ami-hvm-2.0.20211005.0-x86_64-gp2
  bastion_instance_type       = "t2.micro"
  bastion_name_prefix         = "${var.environment}-bastion"
  bastion_root_vol_size       = 30
  bastion_root_vol_type       = "standard"
  bastion_sg_cidrs            = var.allowed_public_ips
  bastion_ssh_key_name        = "${var.environment}-bastion"
  bastion_vpc_id              = var.vpc_id
  bastion_vpc_zone_subnet_ids = var.public_subnets
}


#############################################################################
# NGINX - EC2s, NLB, LISTENER, TARGET GROUP
#############################################################################
// EC2s 
module "nginx" {
  source                    = "./modules/nginx/"
  count                     = 1 # var.enable_app_1 ? 1 : 0
  application               = "${var.environment}-nginx"
  environment               = var.environment
  nginx_asg_desired_size    = 3
  nginx_asg_max_size        = 6
  nginx_asg_min_size        = 3
  nginx_image_id            = "ami-01cc34ab2709337aa" # amzn2-ami-hvm-2.0.20211005.0-x86_64-gp2
  nginx_instance_type       = "t2.micro"
  nginx_name_prefix         = "${var.environment}-nginx"
  nginx_root_vol_size       = 30
  nginx_root_vol_type       = "standard"
  nginx_sg_cidrs            = var.allowed_public_ips
  nginx_ssh_key_name        = "${var.environment}-bastion"
  nginx_vpc_id              = var.vpc_id
  nginx_vpc_zone_subnet_ids = var.private_subnets
}

// EIPs
resource "aws_eip" "nlb1" {
  vpc      = true
}
resource "aws_eip" "nlb2" {
  vpc      = true
}
resource "aws_eip" "nlb3" {
  vpc      = true
}

// NLB
resource "aws_lb" "nlb_nginx" {
    name = "${upper(var.environment)}-${upper(var.project_name)}-LB-NGINX-HTTPS"
    load_balancer_type = "network"
    subnet_mapping {
        subnet_id     = data.terraform_remote_state.vpc.outputs.public_subnets[0]
        allocation_id = aws_eip.nlb1.id
    }
    subnet_mapping {
        subnet_id     = data.terraform_remote_state.vpc.outputs.public_subnets[1]
        allocation_id = aws_eip.nlb2.id
    }
    subnet_mapping {
        subnet_id     = data.terraform_remote_state.vpc.outputs.public_subnets[2]
        allocation_id = aws_eip.nlb3.id
    }
    lifecycle {
        create_before_destroy = true
    }
}

// LISTENER
resource "aws_lb_listener" "https" {
    load_balancer_arn = aws_lb.nlb_nginx.arn
    port = 443
    protocol = "TLS"
    ssl_policy = "ELBSecurityPolicy-FS-1-2-2019-08"
    certificate_arn = var.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg-target-group.arn
  }
}

// TARGET GROUP
resource "aws_lb_target_group" "asg-target-group" {
    name = "${upper(var.env)}-${upper(var.project_name)}-TG-NGINX-NLB-HTTPS"
    port = 443
    protocol = "TLS"
    vpc_id = data.aws_vpc.vpc.id
    # target_type = "instance"

    health_check {
        path                = "/"
        protocol            = "HTTPS"
        # matcher             = "200"
        interval            = 30
        # timeout             = 3
        healthy_threshold   = 3
        unhealthy_threshold = 3
    }
}
