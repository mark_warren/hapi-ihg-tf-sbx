# locals {
#   service_hosted_zone = var.hosted_zone_id
#
# }

# data "aws_route53_zone" "service_hosted_zone" {
#   name         = var.hosted_zone_id
#   private_zone = true
# }

############################################################################
# Create AWS Service discovery - Cloud Map ex. command-api.prod
############################################################################
resource "aws_service_discovery_service" "service_discovery" {
  name = var.service_name

  dns_config {
    namespace_id = var.namespace_id # aws_service_discovery_private_dns_namespace.example.id

    dns_records {
      ttl  = 60
      type = "A"
    }
  }
}

############################################################################
# Create ECS Service
############################################################################
resource "aws_ecs_service" "ecs_service" {
  name            = var.service_name
  cluster         = "hapi-${var.environment}"
  task_definition = aws_ecs_task_definition.service_task.arn
  launch_type     = "FARGATE"
  desired_count   = var.service_desired_task_count
  # health_check_grace_period_seconds  = 180
  deployment_maximum_percent         = var.service_deploy_max_percent
  deployment_minimum_healthy_percent = var.service_deploy_min_percent
  # tags                               = var.required_tags
  platform_version = var.platform_version
  propagate_tags   = "SERVICE"

  #ENABLE THIS ONCE service registry Name Space is created.
  service_registries {
    registry_arn = aws_service_discovery_service.service_discovery.arn
  }

  # Allow external changes without Terraform plan difference. Ex. ASG.
  lifecycle {
    ignore_changes = [desired_count]
  }

  network_configuration {
    subnets          = ["subnet-07c58d633a900d3c5"] # variable
    assign_public_ip = false
    security_groups  = var.security_groups
  }
}

############################################################################
# Create ECS Task Definition
############################################################################
resource "aws_ecs_task_definition" "service_task" {
  family                   = var.service_app_name
  task_role_arn            = "arn:aws:iam::${var.account_id}:role/ecsTaskExecutionRole-${var.environment}"
  execution_role_arn       = "arn:aws:iam::${var.account_id}:role/ecsTaskExecutionRole-${var.environment}"
  network_mode             = "awsvpc"
  cpu                      = var.service_task_cpu
  memory                   = var.service_task_memory
  requires_compatibilities = ["FARGATE"]
  # tags                     = var.required_tags
  container_definitions = var.container_definitions
}

#############################################################################
# Cloudwatch logging
#############################################################################
resource "aws_cloudwatch_log_group" "app_log_group" {
  name = "/ecs/${var.environment}/${var.service_name}"
}
