variable "application" {
  description = "application name. used in resource tags"
  default = "bastion"
}

variable "environment" {}

variable "bastion_asg_desired_size" {
  description = "(Optional) The number of Amazon EC2 instances that should be running in the group."
  default = 1
}

variable "bastion_asg_max_size" {
  description = "(Required) The maximum size of the Auto Scaling Group."
  default = 1
}

variable "bastion_asg_min_size" {
  description = "(Required) The minimum size of the Auto Scaling Group."
  default = 1
}

variable "bastion_image_id" {
  description = "(Required) The EC2 image ID to launch. Used in Launch Configuration"
}

variable "bastion_instance_type" {
  description = "(Required) The size of instance to launch."
  default = "t2.micro"
}

variable "bastion_name_prefix" {
  description = "Prefix to name. Typically same as the env - Prod, Demo, Dev"
}

variable "bastion_root_vol_size" {
  description = "(Optional) The size of the volume in gigabytes."
}

variable "bastion_root_vol_type" {
  description = "(Optional) The type of volume - standard, gp2, gp3, st1, sc1 or io1."
  default = "standard"
}

variable "bastion_sg_cidrs" {
  description = "Publicly accessible IPs"
  type = list
}

variable "bastion_ssh_key_name" {}

variable "bastion_vpc_id" {}

variable "bastion_vpc_zone_subnet_ids" {
  description = "(Optional) A list of subnet IDs to launch resources in. Subnets automatically determine which availability zones the group will reside."
  type = list
}
