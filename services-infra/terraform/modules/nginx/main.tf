resource "aws_launch_configuration" "nginx_launch_cfg" {
  name_prefix          = "${var.nginx_name_prefix}-launch-cfg-"
  image_id             = var.nginx_image_id
  instance_type        = var.nginx_instance_type
  iam_instance_profile = aws_iam_instance_profile.nginx_instance_profile.id

  root_block_device {
    volume_type           = var.nginx_root_vol_type
    volume_size           = var.nginx_root_vol_size
    delete_on_termination = true
  }

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  security_groups = [
    aws_security_group.nginx_sg.id,
  ]

  key_name                    = var.nginx_ssh_key_name #aws_key_pair.ecs_key_pair.key_name
  associate_public_ip_address = "true"
  user_data                   = file(format("%s/%s", path.module, "templates/cloud-init.sh"))
}

resource "aws_autoscaling_group" "nginx_asg" {
  name             = "${aws_launch_configuration.nginx_launch_cfg.name}-asg"
  max_size         = var.nginx_asg_max_size
  min_size         = var.nginx_asg_min_size
  desired_capacity = var.nginx_asg_desired_size
  # availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"]

  vpc_zone_identifier = var.nginx_vpc_zone_subnet_ids

  launch_configuration = aws_launch_configuration.nginx_launch_cfg.id
  health_check_type    = "ELB"

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = var.nginx_name_prefix
  }

  tag {
    key                 = "Application"
    propagate_at_launch = true
    value               = var.application
  }

  tag {
    key                 = "Environment"
    propagate_at_launch = true
    value               = var.environment
  }

  tag {
    key                 = "Terraform"
    propagate_at_launch = true
    value               = "true"
  }
}

resource "aws_iam_role" "nginx_instance_role" {
  name               = "${var.nginx_name_prefix}-instance-role"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.nginx_instance_policy.json
}

resource "aws_iam_role_policy_attachment" "nginx_instance_role_attachment" {
  role       = aws_iam_role.nginx_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "nginx_role_ssm_attachment" {
  role       = aws_iam_role.nginx_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_instance_profile" "nginx_instance_profile" {
  name_prefix = "${var.nginx_name_prefix}-instance-profile"
  path        = "/"
  role        = aws_iam_role.nginx_instance_role.id

  provisioner "local-exec" {
    command = "sleep 10"
  }
}

resource "aws_security_group" "nginx_sg" {
  name_prefix = var.nginx_name_prefix
  vpc_id      = var.nginx_vpc_id
  description = "nginx host security group"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = var.nginx_sg_cidrs
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.nginx_name_prefix}-sg"
    Application = var.application
    Environment = var.environment
    Terraform   = true
  }
}
